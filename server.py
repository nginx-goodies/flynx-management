#! /usr/bin/env python

import sys
import os
from os.path import dirname, abspath

def main(argv):
  if len(argv) == 2:
    if argv[1] == "setup":
      setup()
    elif argv[1] == "run":
      run()
    elif argv[1] == "seed":
      seed()
    elif argv[1] == "database":
      database()
    else: 
      usage()
  elif len(argv) == 3:
    if argv[1] == "hash":
      hash(argv[2])
  else:
    usage()

def setup():
  from flynx import install
  install()
  
def run():
  from flynx import create_app
  app = create_app(config_file())
  app.run(debug=True)
  
def seed():
  from flynx import reset_database
  return reset_database(config_file(), alembic_config())
  
def database():
  from flynx import database_shell
  return database_shell(config_file())
  
def hash(arg):
  from flynx import create_app
  app = create_app(config_file(), False)
  print app.bcrypt.generate_password_hash(arg)
  
def config_file():
  return os.path.join(dirname(abspath(__name__)), 'config.cfg')
  
def alembic_config():
  return os.path.join(dirname(abspath(__name__)), 'alembic.ini')

def usage():
  print """
[-] No such command. 
    Possible commands: 
    
    - run     -> run server 
    - setup   -> setup requirements (if not via install.sh)
    - seed    -> downgrade, then upgrade database to head. Seed data afterwards
    - hash <arg> -> hash arg with bcrypt
      
      """

  
if __name__ == "__main__":
  main(sys.argv)
