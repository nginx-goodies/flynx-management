class QueryError(Exception):
  def __init__(self, status_code, data):
    self.status_code = status_code
    self.data = data
  def __str__(self):
    return repr((self.status_code, self.data))