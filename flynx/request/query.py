import json
import urlparse
import requests

from query_error import QueryError

class Query:
  def __init__(self, domain, path="/", port=80, scheme="http"):
    if not isinstance(port, ( int, long ) ):
      raise Exception("port needs to be integer")
  
    self.domain = "%s:%d" % (domain, port)
    self.scheme = scheme
    self.path = path
    
    if self.path[-1] != "/":
      self.path += "/"
    
    tupel = (self.scheme, self.domain, self.path, "", "", "")
    self.url = urlparse.urlunparse(tupel)
    
    self.callbacks = []
      
  def status(self):
    url = self.__full_url("ping")
    
    ret = self.__get(url)
    self.__callbacks("status", ret["version"], ret)
    
    return ret
  
  def snapshot(self):
    url = self.__full_url("snapshot")
    
    ret = self.__get(url)
    self.__callbacks("snapshot", ret["version"], ret)
    
    return ret
  
  def upstreams(self):
    url = self.__full_url("upstream")
    
    ret = self.__get(url)
    self.__callbacks("upstreams", ret["version"], ret)
    
    return ret
    
  def peers(self, upstream):
    url = self.__full_url("upstream", upstream)
    
    ret = self.__get(url)
    self.__callbacks("status", ret["version"], ret)
    
    return ret
    
  def add_peers(self, upstream, version, peers, defaults={}):
    url = self.__full_url("upstream", upstream)
    data = { "version":version, "peers":peers, "defaults":defaults }
    
    ret = self.__post(url, data)
    self.__callbacks("add_peers", ret["version"], ret)
    
    return ret
  
  def replace_peers(self, upstream, version, peers, defaults={}):
    url = self.__full_url("upstream", upstream)
    data = { "version":version, "peers":peers, "defaults":defaults }
    
    ret = self.__put(url, data)
    self.__callbacks("replace_peers", ret["version"], ret)
    
    return ret
    
  def delete_peers(self, upstream, version, peers):
    url = self.__full_url("upstream", upstream, "delete")
    data = { "version":version, "peers":peers }
    
    ret = self.__post(url, data)
    self.__callbacks("delete_peers", ret["version"], ret)
    
    return ret
    
  def up(self, upstream, version, peers):
    url = self.__full_url("upstream", upstream, "up")
    data = { "version":version, "peers":peers }
    
    ret = self.__post(url, data)
    self.__callbacks("up", None, ret)
    
    return ret
    
  def down(self, upstream, version, peers):
    url = self.__full_url("upstream", upstream, "down")
    data = { "version":version, "peers":peers }
    
    ret = self.__post(url, data)
    self.__callbacks("down", None, ret)
    
    return ret
    
  def add_callback(self, fn):      
    self.callbacks.append(fn)
    
  def __callbacks(self, type, version, data):
    for c in self.callbacks:
      c(type, version, data, json.dumps(data))
    
    
  def __normalize(self, part):
    if part[-1] != "/":
      return part + "/"
    return part
  
  def __full_url(self, *parts):
    return reduce(lambda base, part: urlparse.urljoin(base, self.__normalize(part)), parts, self.url)
    
  def __resolve_request(self, r):
    if r.status_code == 200:
      if r.headers.get("content-type") == "application/json" and r.headers.get("content-length") > 0:
        return r.json()
      else:
        return r.content
        
    if r.status_code == 204:
      return True
      
    data = r.json() if r.headers.get("content-type") == "application/json" else r.content 
    raise QueryError(r.status_code, data)
    
  def __get(self, url):
    r = requests.get(url)
    
    return self.__resolve_request(r)
    
  def __post(self, url, data):
    r = requests.post(url, data=json.dumps(data), headers={'content-type': 'application/json'}) 
    
    return self.__resolve_request(r)
    
  def __put(self, url, data):
    r = requests.put(url, data=json.dumps(data), headers={'content-type': 'application/json'})
    
    return self.__resolve_request(r)
    
  def __delete(self, url):
    r = requests.delete(url)
    
    return self.__resolve_request(r)
    
    
  
  