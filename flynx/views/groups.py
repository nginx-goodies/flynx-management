from flask import current_app, Blueprint, render_template, abort, request, redirect, url_for
from flynx.views import requires_auth
from flynx.model import Group, Node
from flask.json import jsonify

groups = Blueprint('groups', __name__, url_prefix = '/groups')

@groups.route("/")
@requires_auth
def index():
  return render_template("groups/index.html")
  
@groups.route(".json")
@requires_auth
def index_json():
  groups = Group.query.all()
  
  result = map(lambda g: g.to_json(), groups)
  return jsonify(data=result)