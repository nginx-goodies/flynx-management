from flask import current_app, Blueprint, render_template, abort, request, redirect, url_for
from flask.json import jsonify
from flynx.views import requires_auth
from flynx.model import Node, NodeCreateValidator, NodeUpdateValidator

nodes = Blueprint('nodes', __name__, url_prefix = '/nodes')

@nodes.route("/")
@requires_auth
def index():
  return render_template("nodes/index.html")
  
@nodes.route(".json")
@requires_auth
def index_json():
  if request.method == "GET":
    nodes = Node.query.all()
  
    result = map(lambda n : n.to_tree(), nodes)
    return jsonify(data=result)
  
  if request.method == "POST":
    validator = NodeCreateValidator(request.get_json(silent=True))
    result = validator.commit()
    
    if result:
      return jsonify(data=result)
    else:
      return abort(500)
      
  
@nodes.route("/<int:id>.json", methods=["GET", "PUT"])
def show_json(id):
  if request.method == "GET":
    node = Node.query.get(id)
  
    if node:
      result = node.to_tree()
      return jsonify(data=result)
  
    return abort(404)
  if request.method == "PUT":  
    validator = NodeUpdateValidator(request.get_json(silent=True))
    result = validator.commit()
    
    if result:
      return jsonify(data=result)
    else:
      return abort(500)