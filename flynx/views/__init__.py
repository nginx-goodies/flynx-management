from flask import current_app, redirect, url_for, flash, session, request, Response
from functools import wraps
from flynx.model import User

def authenticate():
   """Sends a 401 response that enables basic auth"""
   return Response(
   'Please Login', 401,
   {'WWW-Authenticate': 'Basic realm="Login"'})

def requires_auth(f):
   @wraps(f)
   def decorated(*args, **kwargs):

       auth = request.authorization
       if not auth or not check_login(auth.username, auth.password):
           return authenticate()

       try:
           sess = request.cookies.get('dxid').encode("utf-8")
       except:
           flash('Your sessions seems to have expired, creating new')
           sess = "invalid"

       return f(*args, **kwargs)
   return decorated


def check_login(user, pw):
  u = User.query.filter_by(email=user).first()
  
  return u and u.check_password(pw)
  
from default import default
from groups import groups
from nodes import nodes

__all__ = [ 'default', 'groups', 'nodes' ]
