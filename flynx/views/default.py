from flask import current_app, Blueprint, render_template, abort, request, redirect, url_for
from flynx.views import requires_auth

default = Blueprint('default', __name__, url_prefix = '/')

@default.route("/")
@requires_auth
def index():
  return render_template("default/index.html")