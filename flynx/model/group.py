from flynx.model import db
from flynx.model import Node, Renderable

class Group(db.Model, Renderable):
  __tablename__ = 'groups'
  
  id = db.Column(db.Integer, primary_key=True)
  name = db.Column(db.String(50), nullable=False)
  nodes = db.relationship("Node", lazy='dynamic', backref='group')

  def __init__(self, name, nodes=[]):
    self.name = name
    self.nodes = nodes
    
  def to_json(self):
    return self.tree_node(self.name, children=self.__nodes_json(self.nodes), icon='glyph glyph-more_items')
    
  def __nodes_json(self, input):
    return map(lambda i: i.to_json(), input)