from flynx.model import db
from flask import current_app

class User(db.Model):
  __tablename__ = 'users'
  
  id = db.Column(db.Integer, primary_key=True)
  email = db.Column(db.String(50), nullable=False)
  password = db.Column(db.String(60), nullable=False)
  
  def __init__(self, email, password):
    self.email = email
    self.password = current_app.bcrypt.generate_password_hash(password)
    
  def check_password(self, input):
    return current_app.bcrypt.check_password_hash(self.password, input)