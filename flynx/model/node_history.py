from flynx.model import db

class NodeHistory(db.Model):
  __tablename__ = 'node_histories'
  
  node_id = db.Column(db.Integer, db.ForeignKey('nodes.id'))
  version = db.Column(db.Integer)
  snapshot = db.Column(db.Text)
  
  __table_args__ = (db.PrimaryKeyConstraint('node_id', 'version', name='node_histories_pkey'), {})
  
  def __init__(self, node_id, version, snapshot):
    self.node_id = node_id
    self.version = version
    self.snapshot = snapshot