class Renderable(object):
  def tree_node(self, text, children=[], data={}, icon=False, id='', extra={}):
    return dict({ 'id':id, 'data': data, 'text':text, 'icon': icon, 'children' : children }.items() + extra.items())
    
  def kv_tree_node(self, key, value, children=[], data={}):
    return self.tree_node("%s: %s" % (key, value), data=data, children=children, extra={ 'a_attr': { 'class':'no_checkbox'}})