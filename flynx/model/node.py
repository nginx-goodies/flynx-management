from flynx.request import Query
from flynx.model import db
from flynx.model import Renderable, NodeHistory

class Node(db.Model, Renderable):
  __tablename__ = 'nodes'
  
  id = db.Column(db.Integer, primary_key=True)
  domain = db.Column(db.String(), nullable=False)
  port = db.Column(db.Integer, nullable=True)
  path = db.Column(db.String(), nullable=True)
  scheme = db.Column(db.String(), nullable=True)
  
  last_version = db.Column(db.Integer, nullable=True)
  
  group_id = db.Column(db.Integer, db.ForeignKey('groups.id'), nullable=True)
  
  node_histories = db.relationship("NodeHistory", lazy='dynamic', backref='node')
  
  request_cache = None

  def __init__(self, domain, path='/', port=80, scheme='http'):
    self.domain = domain
    self.path = path
    self.port = port
    self.scheme = scheme
    
  @property
  def request(self):
    if not self.request_cache:
      self.request_cache = Query(self.domain, self.path, self.port, self.scheme)
      self.request_cache.add_callback(self.__callback)
    
    return self.request_cache
    
  def to_json(self):
    result = self.request.snapshot()
    
    return self.__root_json(result)
    
  def to_tree(self):
    result = self.request.snapshot()
    
    return self.__root_json(result)
    
  def __callback(self, type, version, data, string_data):
    if type == "snapshot":
      if not self.query.session.query(self.node_histories.filter_by(version=version).exists()).scalar():
        self.query.session.add(NodeHistory(self.id, version, string_data))
        if version > self.last_version:
          self.last_version = version
        self.query.session.commit()
      
      return
      
    self.request.snapshot()   
    
  def __root_json(self, input):
    return self.tree_node("%s (version: %d)" % (self.domain, self.last_version), data={'id':self.id, 'type':'node'}, children=self.__upstreams_json(input["upstreams"]), icon='glyph glyph-server', id=self.domain)
  
  def __upstreams_json(self, input):
    return map(lambda (k, v): self.__upstream_json(v), input.iteritems())
    
  def __upstream_json(self, input):
    return self.tree_node(input["name"], data={'type':'upstream', 'node_id':self.id, 'upstream':input["name"]}, children=self.__peers_json(input["peers"], input["name"]), icon='glyph glyph-global')
    
  def __peers_json(self, input, upstream):
    return map(lambda i: self.__peer_json(i, upstream), input)
    
  def __peer_json(self, input, upstream):
    return self.tree_node(input["uri"], children=[], icon='glyph-router ' + ("glyph-red" if input["down"] else "glyph-green"), data={'type':'peer', 'node_id':self.id, 'uri':input["uri"], 'upstream':upstream})
    
      