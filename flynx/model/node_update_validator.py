from flynx.model import db, Node

class NodeUpdateValidator:
  def __init__(self, args):
    self.args = args
    self.node = Node.query.get(self.args["node"]["id"])
    
  def validate(self):
    return self.node != None
    
  def commit(self):
    if not self.validate():
      return False
      
    current_version = self.node.last_version + 1
    for k, v in self.args["node"]["upstreams"].iteritems():
      if self.args["type"] == "up":
        self.node.request.up(k, current_version, v["peers"])
      if self.args["type"] == "down":
        self.node.request.down(k, current_version, v["peers"])
        
      current_version += 1
        
    return self.node.to_tree()