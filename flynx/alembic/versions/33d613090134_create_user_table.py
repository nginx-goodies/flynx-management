"""Create user table

Revision ID: 33d613090134
Revises: None
Create Date: 2014-08-01 13:05:25.442270

"""

# revision identifiers, used by Alembic.
revision = '33d613090134'
down_revision = None

from alembic import op
import sqlalchemy as sa


def upgrade():
    op.create_table(
      'users',
      sa.Column('id', sa.Integer, primary_key=True),
      sa.Column('email', sa.String(50), nullable=False, unique=True),
      sa.Column('password', sa.String(60), nullable=False)
    )


def downgrade():
    op.drop_table('users')