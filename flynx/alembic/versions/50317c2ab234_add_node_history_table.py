"""add node_history table

Revision ID: 50317c2ab234
Revises: 14fac773f7eb
Create Date: 2014-08-05 11:54:04.908142

"""

# revision identifiers, used by Alembic.
revision = '50317c2ab234'
down_revision = '14fac773f7eb'

from alembic import op
import sqlalchemy as sa


def upgrade():
    op.create_table(
      "node_histories",
       sa.Column('node_id', sa.Integer, primary_key=True),
       sa.Column('version', sa.Integer, primary_key=True),
       sa.Column('snapshot', sa.Text, nullable=False)
    )
       


def downgrade():
    op.drop_table("node_histories")
