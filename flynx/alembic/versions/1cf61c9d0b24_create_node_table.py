"""create node table

Revision ID: 1cf61c9d0b24
Revises: 33d613090134
Create Date: 2014-08-01 13:53:43.320282

"""

# revision identifiers, used by Alembic.
revision = '1cf61c9d0b24'
down_revision = '33d613090134'

from alembic import op
import sqlalchemy as sa


def upgrade():
    op.create_table(
      'nodes',
      sa.Column('id', sa.Integer, primary_key=True),
      sa.Column('group_id', sa.Integer, nullable=True),
      sa.Column('domain', sa.String(), nullable=False, unique=True),
      sa.Column('port', sa.Integer, nullable=True),
      sa.Column('path', sa.String(), nullable=True),
      sa.Column('scheme', sa.String(), nullable=True),
      sa.Column('last_version', sa.Integer, nullable=True)
    )


def downgrade():
    op.drop_table('nodes')
