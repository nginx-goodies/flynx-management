"""Create group table

Revision ID: 14fac773f7eb
Revises: 1cf61c9d0b24
Create Date: 2014-08-01 14:14:28.103078

"""

# revision identifiers, used by Alembic.
revision = '14fac773f7eb'
down_revision = '1cf61c9d0b24'

from alembic import op
import sqlalchemy as sa


def upgrade():
    op.create_table(
      "groups",
      sa.Column("id", sa.Integer, primary_key=True),
      sa.Column("name", sa.String(50), nullable=False)
    )


def downgrade():
    op.drop_table("groups")
