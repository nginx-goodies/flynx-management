from setuptools.command import easy_install
from time import time 
import os, subprocess, sys

def create_app(config_filename, frontend=True):
  from flask import Flask, app, session, redirect, url_for, escape, request
  from flask.ext.bootstrap import Bootstrap
  from flask.ext.bcrypt import Bcrypt
  from werkzeug.contrib.fixers import ProxyFix
  from flynx.model import db
  import flynx.views
  
  # initiate app
  app = Flask(__name__)
  # proxy-fix for ssl-offloads, fixes bogus http-redirects
  app.wsgi_app = ProxyFix(app.wsgi_app)
  
  # add bcrypt
  app.bcrypt = Bcrypt(app)
  
  # load config
  if config_filename != "":
    app.config.from_pyfile(config_filename)
    
  # init db
  db.init_app(app)

  if frontend:
    print "> app_start"
  
    # add bootstrap css/js
    flynx.bootstrap = Bootstrap(app)
    app.register_blueprint(flynx.views.default, template_folder='templates')
    app.register_blueprint(flynx.views.groups, template_folder='templates')
    app.register_blueprint(flynx.views.nodes, template_folder='templates')
    
  ## end frontend
  
  # TODO: make app_start/seed
  @app.before_request
  def _render_less_css():
      static_dir = app.static_folder
      
      less_paths = []
      
      for path, subdirs, filenames in os.walk(static_dir):       
          less_paths.extend([
              os.path.join(path, f)
              for f in filenames if os.path.splitext(f)[1] == '.less'
              
              
          ])
      for less_path in less_paths:
          css_path = os.path.splitext(less_path)[0] + '.css'
          if not os.path.isfile(css_path):
              css_mtime = -1
          else:
              css_mtime = os.path.getmtime(css_path)
          less_mtime = os.path.getmtime(less_path)
          if less_mtime >= css_mtime:
              app.logger.debug("recompiled "+less_path)
              subprocess.call(['lessc', less_path], stdout=open(css_path, "w"), shell=False)
  
  
  return app

def install():
  for dep in dependencies():
    install_dependency(dep)
    
def install_dependency(dependency):
  easy_install.main([dependency])
  
def reset_database(flaskcfg, cfg):
  from alembic.config import Config
  from alembic import command
  alembic_cfg = Config(cfg)
  command.downgrade(alembic_cfg, "base")
  command.upgrade(alembic_cfg, "head")
  
  return seed_database(flaskcfg)

def seed_database(config_filename):
  app = create_app(config_filename, False)
  
  from sqlalchemy.exc import IntegrityError
  from flynx.model import User, Node, Group
  from flynx.model import db
  
  app.test_request_context().push()
  
  node = Node("localhost", "flynx", 8081)
  
  db.session.add(User("admin@example.com", "hunter2"))
  db.session.add(node)
  db.session.add(Group("test_group", [node]))
  
  try:
    db.session.commit()
  except IntegrityError:
    print "already seeded"
    
def database_shell(config_filename):
  app = create_app(config_filename, False)
  import flynx.model
  from flynx.model import db
  
  app.test_request_context().push()

  import code
  import readline
  import rlcompleter

  vars = globals()
  vars.update(locals())
  readline.set_completer(rlcompleter.Completer(vars).complete)
  readline.parse_and_bind("tab: complete")
  shell = code.InteractiveConsole(vars)
  shell.interact()
  
  
def dependencies():
  return [
    'flask',
    'flask-bootstrap',
    'flask-sqlalchemy',
    'flask-login',
    'flask-bcrypt',
    'flask-wtf',
    'psycopg2',
    'requests',
    'alembic'
  ]