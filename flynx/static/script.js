var flynx = angular.module('flynx', []);

flynx.config(function($interpolateProvider) {
  $interpolateProvider.startSymbol('<<');
  $interpolateProvider.endSymbol('>>');
});

flynx.factory('jstree', ['$rootScope', '$http', function($rootScope, $http) {
  return function(selector, path) {
    var tree = $rootScope.$new();
    
    tree.update = function(result) {    
      if(tree.jstree) {
          tree.jstree.destroy();
        }
        tree.elem = $(selector).jstree({ 'core' : { 'data':result.data}, 'plugins':['checkbox']})
          .on("select_node.jstree", function(e, data) {
            tree.$apply(function(scope) {
              scope.current_node = data.node;
              scope.name = data.node.id;
            })
          })        
          .on("ready.jstree", function(e, data) {
            data.instance.open_all();
          });
        
          tree.jstree = $(tree.elem).jstree(true);
          $rootScope.$broadcast("jstree.refresh");
    }
  
    tree.refresh = function() {
      $http.get(path, {cache:false}).success(tree.update);
    };
        
    tree.checked = function() {
      return tree.jstree.get_bottom_selected(true);
    }
    
    var gen = function(type) {
      return function() {
        var checked = tree.checked();
    
        var nodes = {};
        var node_ids = [];
    
        for(var i = 0; i < checked.length; i++) {
          var local = checked[i];
      
          if(!nodes[local.data.node_id]) {
            nodes[local.data.node_id] = { 'id':local.data.node_id, 'upstreams': {} };
            node_ids.push(local.data.node_id);
          }
      
          if(!nodes[local.data.node_id].upstreams[local.data.upstream]) {
            nodes[local.data.node_id].upstreams[local.data.upstream] = {'upstream':local.data.upstream, 'peers':[]};
          }
      
          nodes[local.data.node_id].upstreams[local.data.upstream].peers.push(local.data.uri);
        }
    
        for(var i = 0; i < node_ids.length; i++) {
          $http({
            url: "/nodes/" + node_ids[i] + ".json",
            method: "PUT",
            data: JSON.stringify({ 'type':type, 'node': nodes[node_ids[i]]}),
            headers: {'Content-Type': 'application/json'}
          }).success(tree.refresh);
        }
      }
    };
    
    
    tree.up = gen("up");  
    tree.down = gen("down");
    
    tree.refresh();
      
    return tree;
    
  }
}]);

flynx.controller("GroupsController", ["$scope", "jstree", function($scope, jstree) {
  $scope.tree = jstree('#js-node-tree', '/groups.json');
}]);

flynx.controller("NodesController", ["$scope", "jstree", function($scope, jstree) {
  $scope.tree = jstree('#js-node-tree', '/nodes.json');
}]);