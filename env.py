#! /usr/bin/env python

import flynx.request

def new_query():
  return flynx.request.Query("localhost", "flynx", 8081)
  
import code
import readline
import rlcompleter

vars = globals()
vars.update(locals())
readline.set_completer(rlcompleter.Completer(vars).complete)
readline.parse_and_bind("tab: complete")
shell = code.InteractiveConsole(vars)
shell.interact()